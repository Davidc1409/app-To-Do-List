import { Component, ElementRef} from '@angular/core';
import { activity } from './activity';
import { ToDoListService } from './to-do-list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-To-Do-List';
  activities : activity[];
  modify :boolean=false;
  updateActivity : activity;
  id :number;
  accomplished : boolean = false;
  
  constructor(private elementRef : ElementRef, private toDoListService : ToDoListService){
    
  }
  
  ngOnInit(){
    this.activities = this.toDoListService.getActivityList();
  }
  
  checkItem(activity : activity){
      if(activity.done=="done"){
        return this.accomplished=true;
      }
      else {
        return this.accomplished=false;
      }
  }

  findId(activity : activity) : number{
    const index = this.activities.findIndex(e => e.name == activity.name );
    return index;
  }

  deleteMustDo(activity : activity){
    const index = this.activities.findIndex(e => e.name == activity.name );
    this.activities.splice(index,1);
    this.toDoListService.setItem(this.activities);
    
  }
  
  Modify(activity : activity){
    this.modify=true;
    this.updateActivity=activity;
    this.elementRef.nativeElement.querySelector("#addUpdate").value=activity.name;
  }

  reset(){
    this.elementRef.nativeElement.querySelector("#addUpdate").value="";
  }
  
  handleInput(name : string){
    if(this.modify){
        const index = this.activities.findIndex(e => e.name == this.updateActivity.name );
        this.activities[index].name=name;
        this.toDoListService.setItem(this.activities);
        this.modify=false;

    }
    else {
      this.activities.push(new activity(name,"undone"));
      this.toDoListService.setItem(this.activities);
    }
    this.reset();
  }

  itemDone(activity : activity){
    const index = this.activities.findIndex(e => e.name == activity.name );
    if(this.activities[index].done != "done"){
      this.activities[index].done="done";
      this.toDoListService.setItem(this.activities);

    }
    else if (this.activities[index].done == "done"){
      this.activities[index].done="undone";
      this.toDoListService.setItem(this.activities);
    }
  }

}
