import { Injectable } from '@angular/core';
import { activity } from './activity';
import { activityList } from './activityList';

@Injectable({
  providedIn: 'root'
})
export class ToDoListService {

  getActivityList () : activity[] {
    return activityList;
  }

  setItem (activities : activity []) : void {
    localStorage.setItem("activityList", JSON.stringify(activities));
  }
}
